

/*******************************************************************
** This code is part of Breakout.
**
** Breakout is free software: you can redistribute it and/or modify
** it under the terms of the CC BY 4.0 license as published by
** Creative Commons, either version 4 of the License, or (at your
** option) any later version.
******************************************************************/
#include "particle_generator.h"
#include <iostream>

ParticleGenerator::ParticleGenerator(Shader shader, Texture2D texture, GLuint amount)
	: shader(shader), texture(texture), amount(amount)
{
	this->init();
}

void ParticleGenerator::Update(GLfloat dt, GameObject &object, GLuint newParticles, glm::vec2 offset)
{
	// Add new particles 
	for (GLuint i = 0; i < newParticles; ++i)
	{
		int unusedParticle = this->firstUnusedParticle();
		std::cout << unusedParticle << std::endl;
		this->respawnParticle(this->particles[unusedParticle], object, offset);
	}
	// Update all particles
	// Remember that the reference to Particle struct which is p is an address not a copy and so will affect Particle stuct and not the copy
	/**************************
	EXPLANATION:
		We have 500 size of vector particles. Most of each item will never be rendered because the life is negative. 
		To be able to render them, we need to make the we need to make them 1.0 but since every paritcle in particles vector will get 1.0 at first and then
		most will be negative only a few of them will be 1.0 and can pass the if condition. The percentage of a particle passing the if condition depends
		on the deltatime being subtracted on a particle that has 1.0 life since most particle get negative value before getting to if condition
	
	*****************************/
	for (GLuint i = 0; i < this->amount; ++i)
	{
		Particle &p = this->particles[i];
		p.Life -= dt; // reduce life
		if (p.Life > 0.0f)
		{	// particle is alive, thus update
			p.Position -= p.Velocity * dt;
			p.Color.a -= dt * 2.5;
		}
	}
}

// Render all particles
void ParticleGenerator::Draw()
{
	// Use additive blending to give it a 'glow' effect
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	this->shader.Use();

	for (Particle particle : this->particles)
	{
		
		if (particle.Life > 0.0f)
		{
		
			this->shader.SetVector2f("offset", particle.Position);
			this->shader.SetVector4f("color", particle.Color);
			this->texture.Bind();
			glBindVertexArray(this->VAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			glBindVertexArray(0);
		}
	}
	// Don't forget to reset to default blending mode
	// When rendering the particles, instead of the default destination blending mode of GL_ONE_MINUS_SRC_ALPHA we use 
	// the GL_ONE blending mode that gives the particles a very neat glow effect when stacked upon each other.
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void ParticleGenerator::init()
{
	// Set up mesh and attribute properties
	GLuint VBO;
	GLfloat particle_quad[] = {
		// position	  Texture coords
		0.0f, 1.0f,   0.0f, 1.0f,
		1.0f, 0.0f,   1.0f, 0.0f,
		0.0f, 0.0f,   0.0f, 0.0f,

		0.0f, 1.0f,   0.0f, 1.0f,
		1.0f, 1.0f,   1.0f, 1.0f,
		1.0f, 0.0f,   1.0f, 0.0f
	};
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(this->VAO);
	// Fill mesh buffer
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(particle_quad), particle_quad, GL_STATIC_DRAW);
	// Set mesh attributes
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);	// combine position and texcoord in one vertex object
	glBindVertexArray(0);

	// Create this->amount default particle instances
	for (GLuint i = 0; i < this->amount; ++i)
	{
		this->particles.push_back(Particle());
	}
		
}

// Stores the index of the last particle used (for quick access to next dead particle)
GLuint lastUsedParticle = 0;
GLuint ParticleGenerator::firstUnusedParticle()
{
	// First search from last used particle, this will usually return almost instantly
	for (GLuint i = lastUsedParticle; i < this->amount; ++i) {
		if (this->particles[i].Life <= 0.0f) {
			lastUsedParticle = i;
			return i;
		}
	}
	// Otherwise, do a linear search
	for (GLuint i = 0; i < lastUsedParticle; ++i) {
		if (this->particles[i].Life <= 0.0f) {
			lastUsedParticle = i;
			return i;
		}
	}
	// All particles are taken, override the first one (note that if it repeatedly hits this case, more particles should be reserved)
	lastUsedParticle = 0;
	return 0;
}


/*************
	This function simply resets the particle's life to 1.0f, randomly gives it a brightness (via the color vector) starting from 
	0.5 and assigns a (slightly random) position and velocity based on the game object.
*************/
void ParticleGenerator::respawnParticle(Particle &particle, GameObject &object, glm::vec2 offset)
{
	GLfloat random = ((rand() % 100) - 50) / 10.0f;
	GLfloat rColor = 0.5 + ((rand() % 100) / 100.0f);

	//// Remember that the reference to Particle struct which is particle is an address not a copy and so will affect Particle stuct and not the copy
	particle.Position = object.Position + random + offset;
	particle.Color = glm::vec4(rColor, rColor, rColor, 1.0f);
	particle.Life = 1.0f;
	particle.id = lastUsedParticle;
	particle.Velocity = object.Velocity * 0.1f;
}

